package Application;

/** Classe */
public interface Objet {

    /** Methode */
    public void afficheElement();

    /** Methode */
    public String getNom();

    /** Methode */
    public Objet getObjet();
}
