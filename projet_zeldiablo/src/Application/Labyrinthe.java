package Application;

import java.util.ArrayList;
import MoteurJeu.Commande;
import MoteurJeu.Jeu;
/** Classe */
public class Labyrinthe implements Jeu{

	/** */
	private ArrayList<Monstre> liste_Monstre;
	/** */
	private CaseLab[][] tab_cases;
	/** */
	private CaseLab depart;
	/** */
	private Aventurier aventurier;
	/** */
	private int taille;
	/** */
	private static boolean finjeu;
	/** */
	private Objet[][] tab_o;

	/** Constructeur */
	public Labyrinthe(){
		int nbCases = 10;
		this.liste_Monstre=new ArrayList<Monstre>();
		this.tab_cases = new CaseLab[nbCases][nbCases];
		this.tab_o = new Objet[nbCases][nbCases];
		this.taille = nbCases;
		for(int i=0; i<nbCases;i++){
			for(int j = 0; j<nbCases; j++ ){
				this.tab_cases[i][j] = new CaseVide(i,j);
			}
		}
		for(int i=0; i<2;i++){
			this.tab_cases[0][i] = new CaseMur(0,i);
		}
		for(int i=4; i<nbCases;i++){
			this.tab_cases[i][0] = new CaseMur(i,0);
		}
		this.tab_cases[3][1] = new CaseMur(3,1);
		this.tab_cases[3][2] = new CaseMur(3,2);
		for(int i=2; i<4;i++){
			this.tab_cases[i][4] = new CaseMur(i,4);
		}
		
		this.tab_cases[5][2] = new CaseMur(5,2);
		this.tab_cases[5][3] = new CaseMur(5,3);
		for(int i=2;i<7;i++){
			this.tab_cases[6][i] = new CaseMur(6,i);
		}
		for(int i=0;i<7;i++){
			this.tab_cases[9][i] = new CaseMur(9,i);
		}
		this.tab_cases[8][9] = new CaseMur(8,9);
		this.tab_cases[8][8] = new CaseMur(8,8);
		for(int i=0;i<7;i++){
			this.tab_cases[i][7] = new CaseMur(i,7);
		}
		this.aventurier = new Aventurier(5,5,10);
		this.genererMonstre();
		this.tab_cases[1][4] = new CaseVide(1, 4);
		this.tab_o[1][4] = new Amulette();
	}

	/** Constructeur */
	public Labyrinthe(int nbCases){
		this.finjeu = false;
		this.liste_Monstre=new ArrayList<Monstre>();
		if (nbCases < 1){
			nbCases = 1;
		}
		this.tab_cases = new CaseLab[nbCases][nbCases];
		this.taille = nbCases;
		this.tab_o = new Objet[nbCases][nbCases];
		this.generationLabyrintheAlea();
		this.tab_cases[(int)nbCases/2][(int)nbCases/2]=new CaseVide((int)nbCases/2,(int)nbCases/2);
		depart = this.tab_cases[(int)nbCases/2][(int)nbCases/2];
		this.aventurier = new Aventurier((int)nbCases/2,(int)nbCases/2, this.taille);
		this.genererMonstre();
		CaseLab c = new CaseVide(1, 4);
		this.tab_cases[1][4] = c;
		this.tab_o[1][4] = new Amulette();
	}

	/** Methode */
	private void genererMonstre() {
		
		int posDepX = 0;
		int posDepY = 0;
		int indiceBoucle =0;
		boolean trouve = false;
		int nbMonstre = 5;
		if (this.tab_cases.length<5){
			nbMonstre = 0;
		}
		for(int k = 0; k<nbMonstre; k++){
			while(!trouve){
				indiceBoucle++;
				posDepX = (int)(Math.random()*this.taille-1);
				posDepY = (int)(Math.random()*this.taille-1);
				if(this.liste_Monstre.size()!=0){
					if(this.tab_cases[posDepY][posDepX].getTypeCase().equals("case vide")){
						for(int i = 0; i<this.liste_Monstre.size(); i++){
							if(this.liste_Monstre.get(i).getX()!=posDepX && this.liste_Monstre.get(i).getY()!=posDepY &&this.aventurier.getX()!=posDepX && this.aventurier.getY()!=posDepY){
								trouve = true;
							}
							else{
								trouve =false;
							}
						}
					}
				}
				else if(this.tab_cases[posDepY][posDepX].getTypeCase().equals("case vide")&&this.aventurier.getX()!=posDepX && this.aventurier.getY()!=posDepY){
					trouve = true;
				}
			}
			double r =Math.random();
			if(r<0.3){
				this.liste_Monstre.add(new MonstreImmo(posDepX, posDepY));	
			}
			else if(r>0.3 && r<0.6){
				this.liste_Monstre.add(new MonstreAleat(posDepX, posDepY));	
			}else{
				this.liste_Monstre.add(new MonstreAttire(posDepX, posDepY));	
			}
			trouve = false;
		}
	}

	/** Methode */
	private void generationLabyrintheAlea(){
		if (this.tab_cases.length>1){
			for(int i=0; i<this.tab_cases.length;i++){
				for(int j = 0; j<this.tab_cases[0].length; j++ ){
					double test = Math.random();
					if (test < 0.6 ||( i==4 && j == 1)){
						this.tab_cases[i][j] = new CaseVide(i,j);
					}
					else{
						this.tab_cases[i][j] = new CaseMur(i,j);
					}
				}
			}
		}
		else{
			this.tab_cases[0][0] = new CaseVide(0,0);
		}
	}

	/** Methode */
	public int getTaille(){
		return this.taille;
	}

	/** Methode */
	public Aventurier getAventurier(){
		return this.aventurier;
	}

	/** Methode */
	public CaseLab[][] getCases(){
		return this.tab_cases;
	}

	/** Methode */
	public CaseLab getDepart(){
		return this.depart;
	}

	/** Methode */
	public ArrayList<Monstre> getListeMonstre(){
		return this.liste_Monstre;
	}
	
	@Override
	/** Methode */
	public void evoluer(Commande commandeUser) {
		if(commandeUser.bas){
			this.deplacementPersonnage("s");
		}
		else if(commandeUser.droite){
			this.deplacementPersonnage("e");
		}
		else if(commandeUser.gauche){
			this.deplacementPersonnage("o");
		}
		else if(commandeUser.haut){
			this.deplacementPersonnage("n");
		}
		else if(commandeUser.espace){
			if (this.liste_Monstre.get(0) !=null){
				aventurier.attaquer(this.liste_Monstre.get(0));
				if(this.liste_Monstre.get(0).etreMort()){
					this.liste_Monstre.remove(0);
				}
			}
		}
	}

	@Override
	/** Methode */
	public boolean etreFini() {
		boolean fini = false;
		if (aventurier.deplacementPossible() == false) {
			fini = true;
		}
		return false;
	}

	/** Methode */
	public static void endGame() {
		if (this.aventurier.possedeAmulette()=true) {
			finjeu = true;
		}
	}

	/** Methode */
	public void ajoutElementInventaire(Objet o){
		this.aventurier.ajoutElementInvetaire(o);
	}

	/** Methode */
	public void afficheContenuInventaire(){
		this.aventurier.afficherContenuInventaire();
	}

	/** Methode */
	public boolean getFinJeu() {
		return finjeu;
	}

	/** Methode */
	public ArrayList<Monstre> getListeMonstreProche(){
		ArrayList<Monstre> tampon = new ArrayList<Monstre>();
		for(Monstre m:this.liste_Monstre){
			if(Math.abs(this.aventurier.getX()-m.getX()) == 1 && Math.abs(this.aventurier.getY()-m.getY()) == 1 ){
				tampon.add(m);
			}
		}
		return tampon;
	}

	/** Methode */
	public Objet[][] getTabObjet() {
		return this.tab_o;
	}

	/** Methode */
	public void deplacementPersonnage(String ch) {
		switch(ch) {
			case "n":
				if (aventurier.getY() - 1 >= 0 && this.tab_cases[aventurier.getY() - 1][aventurier.getX()].getTypeCase().equals("case vide")) {
					aventurier.setY(aventurier.getY() - 1);
				}
				break;
			case "s":
				if (aventurier.getY() + 1 < this.tab_cases.length && this.tab_cases[aventurier.getY() + 1][aventurier.getX()].getTypeCase().equals("case vide")) {
					aventurier.setY(aventurier.getY() + 1);
				}
				break;
			case "e":
				if (aventurier.getX() + 1 < this.tab_cases.length) {
					if (this.tab_cases[aventurier.getY()][aventurier.getX() + 1].getTypeCase().equals("case vide")) {
						aventurier.setX(aventurier.getX() + 1);
					}
				}
				break;
			case "o":
				if (aventurier.getX() - 1 >= 0) {
					if (this.tab_cases[aventurier.getY()][aventurier.getX() - 1].getTypeCase().equals("case vide")) {
						aventurier.setX(aventurier.getX() - 1);
					}
				}
				break;
		}
		if (aventurier.getX() == 1 && aventurier.getY() == 4){
			if (this.tab_o[1][4] != null) {
				aventurier.ajoutAmulette((Amulette) this.tab_o[1][4]);
				this.tab_o[1][4] = null;
				aventurier.afficherContenuInventaire();
			}
		}
	}

	/** Methode */
	public void evolutionJeu() {
		ArrayList<Monstre> lm = this.getListeMonstreProche();
		for(Monstre n:lm){
			n.attaquer(this.aventurier);
		}
		for(Monstre n:this.getListeMonstre()){
			if(n.getTypeMonstre().equals("monstre aleatoire")){
				boolean trouve = false;
				for(Monstre m:lm){
					if(m==n){
						trouve=true;
					}
				}
				if(trouve==false){
					int r = (int)(Math.random());
					if(r<0.25){
						if(n.getX()+1<this.taille){
							if(this.tab_cases[n.getY()][n.getX()+1].getTypeCase().equals("case vide")&&this.aventurier.getX()!=n.getX()+1){
								((MonstreAleat)n).seDeplacer("E");
							}
						}
					}
					else if(r<0.5&&r>0.25){
						if(n.getX()-1>=0){
							if(this.tab_cases[n.getY()][n.getX()-1].getTypeCase().equals("case vide")&&this.aventurier.getX()!=n.getX()-1){
								((MonstreAleat)n).seDeplacer("O");
							}
						}
					}
					else if(r>0.5&&r<0.75){
						if(n.getY()+1<this.taille){
							if(this.tab_cases[n.getY()+1][n.getX()].getTypeCase().equals("case vide")&&this.aventurier.getY()!=n.getY()+1){
								((MonstreAleat)n).seDeplacer("S");
							}
						}
					}
					else if(r>0.75){
						if(n.getY()+1<this.taille){
							if(this.tab_cases[n.getY()-1][n.getX()].getTypeCase().equals("case vide")&&this.aventurier.getY()!=n.getY()-1){
								((MonstreAleat)n).seDeplacer("N");
							}
						}
					}
				}
			}
			if(n.getTypeMonstre().equals("monstre attire")){
				int dx=this.aventurier.getX()-n.getX();
				int dy=this.aventurier.getY()-n.getY();
				int dxAbs=Math.abs(dx);
				int dyAbs=Math.abs(dy);
				if(dx>dy){
					if(dx<0){
						if(n.getX()+1<this.taille){
							if(this.tab_cases[n.getY()][n.getX()+1].getTypeCase().equals("case vide")&&this.aventurier.getX()!=n.getX()+1){
								((MonstreAttire)n).seDeplacer("E");
							}
						}
					}
					else{
						if(n.getX()-1>=0){
							if(this.tab_cases[n.getY()][n.getX()-1].getTypeCase().equals("case vide")&&this.aventurier.getX()!=n.getX()-1){
								((MonstreAttire)n).seDeplacer("O");
							}
						}
					}
				}else{
					if(dy<0){
						if(n.getY()+1<this.taille){
							if(this.tab_cases[n.getY()+1][n.getX()].getTypeCase().equals("case vide")&&this.aventurier.getY()!=n.getY()+1){
								((MonstreAttire)n).seDeplacer("S");
							}
						}
					}else{
						if(n.getY()-1>=0){
							if(this.tab_cases[n.getY()-1][n.getX()].getTypeCase().equals("case vide")&&this.aventurier.getY()!=n.getY()-1){
								((MonstreAttire)n).seDeplacer("N");
							}
						}
					}
				}
			}
		}
	}

}