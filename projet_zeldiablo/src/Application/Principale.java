package Application;

import MoteurJeu.MoteurGraphique;

/** Classe Principale avec la methode main
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 * */
public class Principale {
	/** Methode main qui permet l'affichage d'un labyrinthe */
	public static void main (String[] args) throws InterruptedException{
		Labyrinthe lab = new Labyrinthe(10);
		LabyrintheGraphique lg = new LabyrintheGraphique(lab);
		MoteurGraphique mg = new MoteurGraphique(lab, lg);
		mg.lancerJeu(800, 800);
	}
}