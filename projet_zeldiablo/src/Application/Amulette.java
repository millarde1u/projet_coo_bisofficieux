package Application;

/** Classe Amulette qui implemente l'interface Objet. Cette classe permet de creer une amulette.
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class Amulette implements Objet {

    /** Nom de l'amulette */
    private String nom;

    /** Constructeur de la classe Amulette */
    public Amulette() {
        this.nom = "Amulette";
    }

    /** Methode afficheElement qui permet l'affichage de l'amulette dans la console */
    public void afficheElement() {
        System.out.println(getNom());
    }

    /** Methode getNom qui retourne le nom de l'amulette
     *
     * @return le nom de l'amulette
     */
    public String getNom() {
        return this.nom;
    }

	@Override
    /** Methode getObjet qui retourne */
	public Objet getObjet() {
		return this;
	}

}