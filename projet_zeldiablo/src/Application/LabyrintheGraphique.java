package Application;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import MoteurJeu.DessinJeu;

/** Classe */
public class LabyrintheGraphique implements DessinJeu{

	/** */
	private Labyrinthe lab;

	/** Constructeur */
	public LabyrintheGraphique(Labyrinthe lb){
		this.lab=lb;
	}
	
	@Override
	/** Methode */
	public void dessiner(BufferedImage image) {
		int k = 0;
		java.awt.Graphics g = image.getGraphics();
		int x = image.getHeight();
		int y = image.getWidth();
		ArrayList<Monstre> monstre = this.lab.getListeMonstreProche();
		int nbCase = this.lab.getTaille();
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, nbCase*50, nbCase*50);

		for(int i=0; i<nbCase; i++){
			for(int j=0; j<nbCase; j++){
				if(this.lab.getAventurier().getX()==j && this.lab.getAventurier().getY()==i){
					g.setColor(Color.BLUE);
					g.fillRect(((j*50)+50/2), ((i*50)+50/2), 10, 10);
					if(this.lab.getAventurier().possedeAmulette()){
						g.setColor(Color.YELLOW);
						g.fillOval(((j*50)+27), ((i*50)+27), 6, 6);
					}
				}
				else if(this.lab.getCases()[i][j] instanceof CaseMur){
					g.setColor(Color.GRAY);
					g.fillRect((j*50), (i*50), 50, 50);
				}
				else if(this.lab.getCases()[i][j] instanceof CaseVide) {
					g.setColor(Color.BLACK);
					g.drawRect((j * 50), (i * 50), 50, 50);
				}
				if (this.lab.getTabObjet()[j][i] != null) {
					g.setColor(Color.YELLOW);
					g.fillOval(((j*50)+50/2), ((i*50)+50/2), 10, 10);
				}
			}
		}
		while(k<this.lab.getListeMonstre().size()){
			g.setColor(Color.RED);
			g.fillOval((this.lab.getListeMonstre().get(k).getX()*50)+22, (this.lab.getListeMonstre().get(k).getY()*50)+22, 15, 15);
			k++;
		}
		this.lab.evolutionJeu();
		if (this.lab.getFinJeu()){
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, nbCase*50, nbCase*50);
			g.drawString("GAME OVER", 50, 50);
		}
	}

}