package Application;

public class MonstreAttire extends Monstre{

	/** Constructeur */
	public MonstreAttire(int x, int y) {
		super(x, y);
	}

	/** Methode */
	public void seDeplacer(String dir){
		if(dir.equals("E")){
			this.x=x+1;
		}
		if(dir.equals("O")){
			this.x=x-1;
		}
		if(dir.equals("N")){
			this.y=y+1;
		}
		if(dir.equals("N")){
			this.y=y-1;
		}
	}

	/** Methode */
	public String getTypeMonstre(){
		return "monstre attire";
	}

}