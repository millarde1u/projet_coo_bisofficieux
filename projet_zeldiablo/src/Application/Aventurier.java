package Application;

/** Classe Aventurier qui herite de la classe abstraite Personnage. Cette classe va permettre de creer des aventuriers.
 *
 * @author GwendolynV
 * @author S_Camille
 * @author millarde1u
 * @author Maxime57
 */
public class Aventurier extends Personnage{

	/** */
	private int tailleLab;
	/** */
	private boolean deplacement;
	/** */
	private Inventaire inventaire;
	/** */
	private boolean attaque;
	/** */
	private Amulette amulette;

	/** Constructeur */
	public Aventurier(int ax, int ay, int taille) {
		this.x = ax;
		this.y = ay;
		this.tailleLab = taille;
		this.deplacement = true;
		this.pv = 5;
		this.mort = false;
		inventaire = new Inventaire();
		this.attaque = false;
		this.amulette = null;
	}

	/** Methode */
	public boolean deplacementPossible() {
		if(y==this.tailleLab || x == this.tailleLab){
			deplacement = false;
		}
		return deplacement;
	}

	/** Methode */
	public void ajoutElementInvetaire(Objet o){
		this.inventaire.ajoutElementInventaire(o);
	}

	/** Methode */
	public void afficherContenuInventaire(){
		this.inventaire.afficheContenuInventaire();
	}

	/** Methode */
	public void subirDegats(int d){
		this.pv = this.pv - d;
		if (this.pv <= 0){
			this.pv = 0;
			this.etreMort();
			Labyrinthe.endGame();
		}
	}
	
	@Override
	/** Methode */
	public void attaquer(Personnage p) {
		if (p!=null) {
			if (!p.etreMort()) {
				p.subirDegats(1);
			}
		}
	}

	/** Methode */
	public Objet[] getElement(){

		return inventaire.getElement();
	}

	/** Methode */
	public void ajoutAmulette(Amulette amu) {
		this.amulette = amu;
	}

	/** Methode */
	public boolean possedeAmulette(){
		return !(this.amulette==null);
	}

}