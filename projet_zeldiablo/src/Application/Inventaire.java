package Application;

import java.util.ArrayList;

/** Classe Inventaire qui permet de creer un inventaire
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class Inventaire {

    /** */
    private ArrayList<Objet> liste_objets;

    /** Constructeur de la classe Inventaire */
    public Inventaire(){
        liste_objets=new ArrayList<Objet>();
    }

    /** Methode ajoutElementInventaire qui permet d'ajouter un objet a un inventaire
     *
     * @param o
     *      Objet que l'on veut ajouter a l'inventaire
     */
    public void ajoutElementInventaire(Objet o){
        this.liste_objets.add(o);
    }

    /** Methode afficheContenuInventaire qui permet d'afficher le contenu d'un inventaire */
    public void afficheContenuInventaire(){
        for(Objet n:this.liste_objets){
            n.afficheElement();
        }
    }

    /** Methode getElement qui retourne un tableau d'objet
     *
     * @return un tableau d'objet
     */
    public Objet[] getElement(){
        Objet[] s = new Objet[50];
        int i = 0;
        for (Objet n:this.liste_objets){
            s[i] = n;
            i++;
        }
        return s;
    }
}