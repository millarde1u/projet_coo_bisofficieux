package Application;

public class MonstreImmo extends Monstre{

	/** Constructeur */
	public MonstreImmo(int x, int y) {
		super(x, y);
	}

	/** Methode */
	public String getTypeMonstre(){
		return "monstre Immobile";
	}

}