package Application;

/** Classe Monstre qui herite de la classe abstraite Personnage. Cette classe ermet de creer des monstres.
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class Monstre extends Personnage{
	
	/** Constructeur */
	public Monstre(int x, int y){
		this.x=x;
		this.y=y;
		this.pv = 2;
	}
	
	@Override
	/** Methode */
	public void attaquer(Personnage p) {
		if (!p.etreMort()){
			p.subirDegats(1);
		}
	}

	/** Methode */
	public void subirDegats(int d){
		this.pv = this.pv - d;
		if (this.pv <= 0){
			this.pv = 0;
			this.etreMort();
		}
	}

	/** Methode */
	public String getTypeMonstre(){
		return "monstre";
	}

}