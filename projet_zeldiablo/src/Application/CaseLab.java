package Application;

/** Interface CaseLab qui va permettre la creation de case
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public interface CaseLab {

	/** Methode getX qui retourne l'abscisse de la case
	 *
	 * @return l'abscisse de la case
	 */
	public int getX();
	/** Methode getY qui retourne l'ordonnee de la case
	 *
	 * @return l'ordonnee de la case
	 */
	public int getY();

	/** Methode getTypeCase qui retourne le type de la case, soit elle est traversable (case vide), soit elle ne l'est pas (case mur)
	 *
	 * @return le type de la case, soit elle est traversable (case vide), soit elle ne l'est pas (case mur)
	 */
	public String getTypeCase();
	
}