package Application;

/**
 * Classe CaseMur qui implemente l'interface CaseLab. Cette classe permet la creation de cases murs.
 *
 * @author S_Camille
 * @author GwendolynV
 * @author millarde1u
 * @author Maxime57
 */
public class CaseMur implements CaseLab{

	/** Abscisse de la case */
	private int positionX;
	/** Ordonnee de la case */
	private int positionY;

	/**
	 * Constructeur de la classe CaseMur, qui cree une case mur
	 *
	 * @param x
	 * 		Abscisse de la case
	 * @param y
	 * 		Ordonnee de la case
	 */
	public CaseMur(int x, int y){
		this.positionX = x;
		this.positionY = y;
	}

	/**
	 * Methode getX qui retourne l'abscisse de la case
	 *
	 * @return l'abscisse de la case
	 */
	public int getX(){
		return this.positionX;
	}

	/**
	 * Methode getY qui retourne l'ordoneee de la case
	 *
	 * @return l'ordonnee de la case
	 */
	public int getY(){
		return this.positionY;
	}

	/**
	 * Methode getTypeCase qui retourne la chaine "case mur"
	 *
	 * @return la chaine "case mur"
	 */
	public String getTypeCase(){
		return "case mur";
	}

}