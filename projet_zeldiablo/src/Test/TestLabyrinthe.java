package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import Application.CaseLab;
import Application.Labyrinthe;
import Application.Aventurier;

public class TestLabyrinthe {

	@Test
	public void testConstructeurAvecBonParametre() {
		Labyrinthe l=new Labyrinthe(10);
		CaseLab[][] cases=l.getCases();
		int nbCases = cases.length;
		assertEquals("Le nombre de case devrait devait etre de 4", 10, nbCases);
		assertEquals("Le personnage crée n'est pas un aventurier", true, l.getAventurier() instanceof Aventurier);
		assertEquals("Le Depart devrait etre sur une case libre","case vide",  l.getDepart().getTypeCase() );
	}
	
	@Test
	public void testConstructeurAvecMauvaiParametre() {
		Labyrinthe l=new Labyrinthe(-42);
		CaseLab[][] cases=l.getCases();
		int nbCases = cases.length * cases[0].length;
		assertEquals("Le nombre de case devrait devait etre de 1", nbCases,1);
		assertEquals("Le personnage crée n'est pas un aventurier",l.getAventurier() instanceof Aventurier, true);
		assertEquals("Le Depart devrait etre sur une case libre", l.getDepart().getTypeCase(),"case vide" );
	}

}
