package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import Application.*;


public class TestMonstre {
	
	@Test
	public void testAttaquerMort(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		for(int i=0; i<6; i++){
			monstre.attaquer(lab.getAventurier());
		}
		
		assertEquals("le heros est mort", true, lab.getAventurier().etreMort());
	}
	
	@Test
	public void testAttaquerVivant(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		monstre.attaquer(lab.getAventurier());
		
		assertEquals("le heros est mort", false, lab.getAventurier().etreMort());
	}
	
	
	@Test 
	public void testSubirDegat(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		monstre.subirDegats(1);

		assertEquals("le monstre a perdu un pv", 1, monstre.getPv());
		assertEquals("le monstre n'est pas mort", false, monstre.etreMort());
		
	}
	
	@Test
	public void testSubirDegatMort(){
		Labyrinthe lab = new Labyrinthe();
		Monstre monstre = new Monstre(lab.getAventurier().getX() + 1,lab.getAventurier().getY());
		
		monstre.subirDegats(3);
		
		assertEquals("le monstre a zero pv", 0, monstre.getPv());
		assertEquals("le monstre est mort", true, monstre.etreMort());
	}
	

}
