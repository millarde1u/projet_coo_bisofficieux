package Test;

import static org.junit.Assert.*;
import org.junit.Test;

import Application.CaseLab;
import Application.CaseMur;
import Application.CaseVide;

public class TestCaseLab {
	
	
	@Test
	/**
	 * test si le constructeur de caseLab initialise correctement les cases
	 */
	public void testConstructeur(){
		
		//initialisation
		CaseLab cmur= new CaseMur(2, 3);
		CaseLab cvide = new CaseVide(2, 3);
		
		//verification
		assertEquals("la case mur est bien plac� en abscisse", 2, cmur.getX());
		assertEquals("la case mur est bien plac� en ordonnee", 3, cmur.getY());
		assertEquals("la case vide est bien plac� en abscisse", 2, cvide.getX());
		assertEquals("la case vide est bien plac� en ordonnee", 3, cvide.getY());
	}

}
