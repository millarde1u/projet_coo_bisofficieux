package Test;

import static org.junit.Assert.*;

import org.junit.Test;
import Application.*;

public class TestInventaire {

	@Test
	public void test() {
		Aventurier a=new Aventurier(6,6,10);
		Amulette am=new Amulette();
		a.ajoutElementInvetaire(am);
		Objet[] n=new Objet[1];
		n[0] = am;
		assertEquals("L'objet devrait etre une amulette", am, n[0]);
	}

}
