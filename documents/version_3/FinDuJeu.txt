@startuml
participant "l:Labyrinthe" as l
participant "m:Monstre" as m
l -> m : attaquer(a)
activate m
participant "a:Aventurier" as a
m -> a : attaquer()
activate a
m <-- a
deactivate a
l <-- m
deactivate m
l -> a : etreMort()
activate a
l <-- a : true
deactivate a
l -> l : endGame()
@enduml