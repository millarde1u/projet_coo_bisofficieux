@startuml
create main
create "l:Labyrinthe" as l
main -> l : new
create "a:Aventurier" as a
l -> a : new(4,4)
create "m:Monstre" as m
loop 10
l -> m : new
end
@enduml