@startuml

create "l:Labyrinthe" as l
create "a:Aventurier" as a
create "e:Inventaire" as e
a-> e : new
create "am:Amulette" as am
l-> am : new(posX, posY) 
l-> a : ajoutElementInventaire(Objet)
activate a
a->e : AjoutElementInventaire(Objet)
activate e
a<--e
deactivate e
l <-- a 
deactivate a
l -> a : afficheContenuInventaire()
activate a
a -> e : afficheContenuInventaire()
activate e
a <-- e
deactivate e
l <-- a 
deactivate a

@enduml
